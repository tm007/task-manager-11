package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommand();

}
