package ru.tsc.apozdnov.tm.repository;

import ru.tsc.apozdnov.tm.api.repository.ICommandRepository;
import ru.tsc.apozdnov.tm.constant.ArgumentConstant;
import ru.tsc.apozdnov.tm.constant.TerminalConstant;
import ru.tsc.apozdnov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConstant.ABOUT, ArgumentConstant.ABOUT,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.HELP,
            "Show terminal commands."
    );

    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.INFO,
            "Show system info."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS,
            "Show list arguments."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, ArgumentConstant.COMMANDS,
            "Show list commands."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConstant.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConstant.TASK_CLEAR, null,
            "Remove all task."
    );

    private static final Command TASK_CREAT = new Command(
            TerminalConstant.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command PROJECT_CREAT = new Command(
            TerminalConstant.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConstant.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConstant.PROJECT_CLEAR, null,
            "Remove all project."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConstant.TASK_REMOVE_BY_ID, null,
            "Remove task by ID."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConstant.TASK_REMOVE_BY_INDEX, null,
            "Remove task by INDEX."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConstant.TASK_UPDATE_BY_ID, null,
            "Update task by ID."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConstant.TASK_UPDATE_BY_INDEX, null,
            "Update task by INDEX."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConstant.TASK_SHOW_BY_ID, null,
            "Show task by ID."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConstant.TASK_SHOW_BY_INDEX, null,
            "Show task by INDEX."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConstant.PROJECT_SHOW_BY_INDEX, null,
            "Show project by INDEX."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConstant.PROJECT_SHOW_BY_ID, null,
            "Show project by ID."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by INDEX."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_ID, null,
            "Remove project by ID."
    );

    private static final Command PROJECT_UPDATE_BY_INIDEX = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by INDEX."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_ID, null,
            "Update project by ID."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null,
            "Close application Task Manager."
    );

    private static Command[] terminalCommands = new Command[]{
            ABOUT, VERSION, HELP, INFO,
            ARGUMENTS, COMMANDS,
            PROJECT_CLEAR, PROJECT_CREAT, PROJECT_LIST,
            TASK_CLEAR, TASK_LIST, TASK_CREAT,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INIDEX,
            EXIT
    };

    @Override
    public Command[] getCommands() {
        return terminalCommands;
    }

}
